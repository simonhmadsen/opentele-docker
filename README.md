# README #

This README documents the content of this repository along with the steps needed to use the files contained within this repository
### What is this repository for? ###

This repository contains the docker- and docker-compose files needed to set up a development version of the Opentele clinician server, the Opentele citizen server and the Opentele Client html.

The source code for all three abovementioned projects can be found here:
https://bitbucket.org/4s/

### How do I get set up? ###

* Install a docker and docker-compose on your local machine
* Clone the contents of this repository to a local folder (or copy the docker-compose.yml file)
* In that folder execute the command docker-compose up 
* Once all files have been downloaded, and three components have been started, access http://localhost:8082/manager
* Type in admin for username and password
* Find the Opentele-citizen-server and press the "Start"-button, if it is not already running
* Wait until the component has started in the manager
* All three components should now be running on the following URL's:
    * Opentele clinician server: http://localhost:8081/opentele-server
    * Opentele citizen server:   http://localhost:8082/opentele-citizen-server
    * Opentele client html:      http://localhost:8083/opentele-client-html


### Who do I talk to? ###

* These files have been created by The Alexandra Institute
* If you have any questions, contact Simon H. Madsen at madsen@alexandra.dk